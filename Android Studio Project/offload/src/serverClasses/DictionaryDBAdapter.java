package serverClasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DictionaryDBAdapter {

	private Connection connection;
	private final String DB_PATH = "dictionary.db";
	
	
	static { 
        try { 
            Class.forName("org.sqlite.JDBC"); 
        } catch (ClassNotFoundException e) { 
            System.err.println("Fehler beim Laden des JDBC-Treibers"); 
            e.printStackTrace(); 
        } 
    } 
	
	public void initDBConnection() { 
        try { 
            if (connection != null) 
                return; 
            System.out.println("Creating Connection to Database..."); 
            String path = this.getClass().getResource("dictionary.db").getPath(); 
            connection = DriverManager.getConnection("jdbc:sqlite:" + path); 
            if (!connection.isClosed()) 
                System.out.println("...Connection established"); 
        } catch (SQLException e) { 
            throw new RuntimeException(e); 
        } 

        Runtime.getRuntime().addShutdownHook(new Thread() { 
            public void run() { 
                try { 
                    if (!connection.isClosed() && connection != null) { 
                        connection.close(); 
                        if (connection.isClosed()) 
                            System.out.println("Connection to Database closed"); 
                    } 
                } catch (SQLException e) { 
                    e.printStackTrace(); 
                } 
            } 
        }); 
    } 
	
	public String dbQuery(String text, String from, String to) throws SQLException {
        String sql = "SELECT " + to + " FROM deen WHERE " + from + " LIKE '%" + text + "%' ";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        if (!rs.next()) {
        	return null;
        }
        connection.close();
        return rs.getString(to);
    }
	
	
}
