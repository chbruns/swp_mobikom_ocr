package serverClasses;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import com.memetix.mst.language.Language;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Test extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		doPost(request, response);
	}

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/xml");
        PrintWriter out = response.getWriter();
        String nLoopsStr = request.getParameter("nLoops");
        long startTime = System.currentTimeMillis();
        long nLoops = 0;
        if (nLoopsStr != null) nLoops = Long.parseLong(nLoopsStr) * 1000000;
		long i = 0;
		while (i < nLoops) i++;
		long takenTime = System.currentTimeMillis() - startTime;
		
		out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		out.println("<root>");
		out.println("	<sympathicAnswer>Server says: I have finished looping!</sympathicAnswer>");
		out.println("	<osName>" + System.getProperty("os.name") + "</osName>");
		out.println("	<osArch>" + System.getProperty("os.arch") + "</osArch>");
		out.println("	<javaVersion>" + System.getProperty("java.version") + "</javaVersion>");
		out.println("	<loopsNumber>" + i + "</loopsNumber>");
		out.println("	<executionTime>" + takenTime + "</executionTime>");
		out.println("</root>");
		
		// testing translation

    	Translator translator = new Translator();
    	try {
			String test1 = translator.translate("Test eins, zwei, drei", Language.GERMAN, Language.ENGLISH);
			String test2 = translator.translate("Test", Language.GERMAN, Language.ENGLISH);
			String test3 = translator.translate("Test", Language.ENGLISH, Language.GERMAN);
			String test4 = translator.translate("Woobiedu", Language.ENGLISH, Language.GERMAN);
			System.out.println(test1 + "," + test2 + "," + test3 + "," + test4);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
}
