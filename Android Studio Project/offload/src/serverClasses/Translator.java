package serverClasses;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

public class Translator {
	
	
	private static final String CLIENT_ID = "MSI-Translator";
    private static final String CLIENT_SECRET = "4gIvNg08mnbOuNofn+GxCQSWOyetmco26s6gnQu5owc=";
	private DictionaryDBAdapter mDbHelper;
    
    public Translator() {
    	mDbHelper = new DictionaryDBAdapter();
    	mDbHelper.initDBConnection();
    	
    	
    }
    
    
    
    public String translate(String toTranslate, Language from, Language to) throws SQLException{
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(toTranslate.trim());						// ADD trim
        boolean found = matcher.find();

        // if toTranslate is one word try to use offline dictionary, if offline-translation not possible continue with online-api
        // else use MS-Translator-API
        if (!found) {
        	if( (from.toString().equals("en") && to.toString().equals("de")) ||  (from.toString().equals("de") && to.toString().equals("en"))) {
        		return translateOffline(toTranslate, from, to);
        	}else {
        		return translateOnline(toTranslate, from, to);
        	}	
        } else {
            return translateOnline(toTranslate, from, to);
        }
    }
    
    private String translateOffline(String toTranslate, Language from, Language to) throws SQLException {
    	String translated;
    	translated = mDbHelper.dbQuery(toTranslate.trim(), from.name().toLowerCase(), to.name().toLowerCase());		//ADD trim
		if (translated == null) {
			return translateOnline(toTranslate, from, to);
		} else {
			return translated;
		}
    	 
	}

	private String translateOnline(String toTranslate, Language from, Language to) {
        Translate.setClientId(CLIENT_ID);
        Translate.setClientSecret(CLIENT_SECRET);

        String translatedText;
        try {
            translatedText = Translate.execute(toTranslate.trim(), from, to);
        } catch(Exception e) {
            translatedText = e.toString();
        }
        return translatedText;
    }

}
