package serverClasses;



import java.io.File;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.lang.Runtime;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.memetix.mst.language.Language;



/**
 * Servlet implementation class UploadImage
 */
@WebServlet("/UploadImage")
public class UploadImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadImage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	/*			
			ServletContext context = getServletContext();
			String dir = context.getRealPath(File.separator) + "ocr" + File.separatorChar + "temp" + File.separatorChar;
	       //èŽ·å�–è¾“å…¥æµ�ï¼Œæ˜¯HTTPå��è®®ä¸­çš„å®žä½“å†…å®¹   
	       ServletInputStream inn = request.getInputStream();   
	       //ç¼“å†²åŒº   
	       byte bufferr[]=new byte[1024]; 
	       File log = new File(dir, "text.log");
	       FileOutputStream out=new FileOutputStream(log);   
	       int lenn = inn.read(bufferr, 0, 1024);   
	       //æŠŠæµ�é‡Œçš„ä¿¡æ�¯å¾ªçŽ¯è¯»å…¥åˆ°file.logæ–‡ä»¶ä¸­   
	       while( lenn !=-1 ){   
	           out.write(bufferr, 0, lenn);   
	           lenn = inn.readLine(bufferr, 0, 1024);   
	  
	       }   
	       out.close();   
	       inn.close();  
		
		
	
 		
		String device = request.getParameter("devicekey");
		int type = 0;
		if (device == "httpclient")
		{	type = 1;	}
		else if (device == "mobile")
		{	type = 2; }	
*/	

		long receivedTime = System.nanoTime();
		long receivedfinishTime = 0;
		long conversionfinished = 0;
		long deletefinished = 0;
		long returnTime = 0;
		long receivedPeriod;
		long returnPeriod;
		String prefix = "";
		int type = 0;
		String tessa_from = "";					// ADD, hier wird die bereits umgeformte eng, deu, usw. Bezeichnung für die tessaract Option -l (Auswahl der Zielsprache) gespeichert.
        String from = "";						// ADD, hier wird das enum-Kürzel stehen (en,de, usw.)
		String to = "";							// ADD, hier wird das enum-Kürzel stehen (en,de, usw.)
		String original_text = "";				// ADD
		String translated_text = "";			// ADD
		
		String devicetype = "devicekey";
		String deskclient = "httpclient";
		String mobileclient = "mobile";
		ServletContext context = getServletContext();
		String dir = context.getRealPath(File.separator) + "ocr" + File.separatorChar + "temp" + File.separatorChar;
		File file;
		String imagename = "The_Image";
		String transimagename = "The_Image.tif";
		String result = "Character";
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		StringBuffer sb = new StringBuffer();
		Process changeprocess = null;
		Process process = null;
		
		try{
			List<FileItem> fields = null;
			try {
				fields = upload.parseRequest(request);
			} catch (FileUploadException e1) {
				e1.printStackTrace();
			}
			Iterator<FileItem> it = fields.iterator();
			while(it.hasNext())
			{	
				FileItem fileItem = it.next();
				if(fileItem.isFormField()){
					String name = fileItem.getFieldName();
					String value = fileItem.getString();
					value = new String(value.getBytes("iso-8859-1"),"utf-8");
					if(name.equals(devicetype))
					{
						if (value.equals(deskclient))
						{	type = 1;	}
						else if (value.equals(mobileclient))
						{	type = 2; }	
					}
					
					if(name.equals("from")) {																// ADD 
						from = value;
						if(from.equals("en")) tessa_from = "eng";
						if(from.equals("de")) tessa_from = "deu";
						if(from.equals("es")) tessa_from = "spa";
						if(from.equals("fr")) tessa_from = "fra";
						if(from.equals("it")) tessa_from = "ita";
					}
					
					if(name.equals("to")) {																	// ADD
						to = value;
					}
					
					//System.out.println("from: "+from+", to: "+to);
					
					
				} else {
					String filename = fileItem.getName();
					prefix = filename.substring(filename.lastIndexOf(".")+1);
					imagename += ".";
					imagename += prefix;
					file = new File(dir, imagename);
					file.createNewFile();
					InputStream in = fileItem.getInputStream();
					FileOutputStream fos = new FileOutputStream(file);
					
					int len;
					byte[] buffer = new byte[1024];
					while ((len = in.read(buffer))>0)
						fos.write(buffer, 0, len);
						
					fos.close();
					in.close();
					
					fileItem.delete();
//					Tesseract instance = Tesseract.getInstance();
//					String result = instance.doOCR(file);
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		
		receivedfinishTime = System.nanoTime();	
		
		try{
			File content = new File(dir, result + ".txt");
			content.delete();
//			out.print("<p> The image has been deleted. </p>");				
			}catch(Exception e){
//				out.print("<p> Cannot delet the image! </p>");
			}
		deletefinished = System.nanoTime();
		/*
		if(!prefix.equals("tif"))
		{
			String change = "/usr/bin/convert -colors 256 -alpha off " + dir + imagename + " " + dir + transimagename;
//			try{
			changeprocess = Runtime.getRuntime().exec(change);
			BufferedReader cr = new BufferedReader(new InputStreamReader(changeprocess.getInputStream()));
			String changeline;
				while((changeline = cr.readLine()) != null){
					sb.append(changeline).append("<br/>");
				}
//			changeprocess.waitFor();
	//		changeprocess.destroy();
	//		}catch(Exception e){
				
	//		}
			
		}
		*/
		conversionfinished = System.nanoTime();
		
		String command = null;
		if (System.getProperty("os.name").startsWith("Mac OS")) {
			command = "/opt/local/bin/tesseract " + dir + transimagename + " " + dir + result;
		} else {
			String option = "";																				// ADD OPTION
			if(!tessa_from.equals("")) option += " -l "+ tessa_from;										// ADD OPTION
			command = "tesseract " + dir + transimagename + " " + dir + result + option;					// ADD OPTION
		}
		
		
		
//		try{
		process = Runtime.getRuntime().exec(command);
		BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));

		String line;
          while ((line = br.readLine()) != null) {
             sb.append(line).append("<br/>");
          }
//        process.waitFor();
//        process.destroy();
//		}catch(Exception e){
			
//		}
			result += ".txt";  
		returnTime = System.nanoTime();		
		
//		response.sendRedirect("/offload/ocr/resultshow.jsp");
		if (type == 1){
			request.getSession().setAttribute("imagename", imagename);
			request.getSession().setAttribute("Directory", dir);  		            
            request.getSession().setAttribute("Infos", sb.toString());
			request.getSession().setAttribute("textcontent", result);
			RequestDispatcher dispatcher = request.getRequestDispatcher("./resultshow.jsp");
			dispatcher.forward(request, response);
		}else{
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			out.println("<root>");
			out.println("	<sympathicAnswer>Server says: I have received the message!</sympathicAnswer>");
			out.println("	<osName>" + System.getProperty("os.name") + "</osName>");
			out.println("	<osArch>" + System.getProperty("os.arch") + "</osArch>");
			out.println("	<javaVersion>" + System.getProperty("java.version") + "</javaVersion>");
			String RCTime = String.valueOf(receivedTime);
			out.println("	<ReceivedTime>" + RCTime + "</ReceivedTime>");
			receivedPeriod = receivedfinishTime - receivedTime;
			String SVTime = String.valueOf(receivedPeriod);
			out.println("	<ReceivedPeriod>" + SVTime + "</ReceivedPeriod>");			
			String RFTime = String.valueOf(receivedfinishTime);
			out.println("	<ReceivedFinish>" + RFTime + "</ReceivedFinish>");
			String DETime = String.valueOf(deletefinished);
			out.println("	<DeleteTime>" + DETime + "</DeleteTime>");
			String COTime = String.valueOf(conversionfinished);
			out.println("	<ConversionTime>" + COTime + "</ConversionTime>");
			String TUTime = String.valueOf(returnTime);
			out.println("	<ReturnTime>" + TUTime + "</ReturnTime>");
			long remotime = returnTime - receivedfinishTime;
			String RTTime = String.valueOf(remotime);
			out.println("	<RemotePeriod>" + RTTime + "</RemotePeriod>");
		    out.println("	<FromLanguage>"+ from + "</FromLanguage>");					// ADD
			out.println("	<ToLanguage>"+ to + "</ToLanguage>");						// ADD
			
			out.println("	<Identifiedtext>");
			try{
				File f = new File(dir, result);
				FileInputStream inchar = new FileInputStream(f);
				BufferedInputStream buffer=new BufferedInputStream(inchar);
				byte c[]=new byte[90];
				int n=0;
				while((n=buffer.read(c))!=-1)
				{	String temp=new String(c,0,n);
	//				out.print("<p>"+ temp + "</p>");
					original_text += temp + "";													// ADD
					out.println(temp + " ");
				}

				buffer.close();
			}
			catch(IOException e){
//				out.print("<p> No result </p>");
				out.print(" No result! ");				
			}
			out.println("</Identifiedtext>");
			
			// HIER WIRD ÜBERSETZT
			
			if(!to.equals("none")) {
		    	Translator translator = new Translator();
		    	try {
		    		translated_text = translator.translate(original_text, Language.fromString(from), Language.fromString(to));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			out.println("<Translatedtext>"+ translated_text +"</Translatedtext>");			// ADD
			long returnfinishTime = System.nanoTime();
			String TFTime = String.valueOf(returnfinishTime);
			out.println("	<ReturnFinish>" + TFTime + "</ReturnFinish>");
			returnPeriod = returnfinishTime - returnTime;
			String RETime = String.valueOf(returnPeriod);
			out.println("	<ReturnPeriod>" + RETime + "</ReturnPeriod>");
			out.println("</root>");
			
		}
		
	}
	
}