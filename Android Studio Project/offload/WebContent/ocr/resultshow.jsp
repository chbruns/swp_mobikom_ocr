<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/style.css" media="screen" rel="Stylesheet" type="text/css">
<title>OCR Recognition Result</title>
</head>
<body>
		<div id="container">
		<div id="header">
			<h1>Android mobile devices computation offloading project</h1>
		</div>
		<div id="navigation">
			<ul>
				<li><a href="../index.jsp">Home</a></li>
				<li><a href="../management">Management</a></li>
				<li><a href="./">Ocr test</a></li>			
				<li><a href="../downloads">Downloads</a></li>
				<li><a href="../test" rel="nofollow">Test1</a></li>
				<li><a href="../run?algName=doSomeLoops&param1=20000000" rel="nofollow">Test2</a></li>
				<li><a href="../contact.jsp">Contact us</a></li>
				<li><a href="../logout">Logout</a></li>
			</ul>
		</div>
		
		<div id="container">
			<h2>OCR Recognition Result</h2>
			<p>Your uploaded image is:</p>
			<% String name=(String)session.getAttribute("imagename");
			   String result=(String)session.getAttribute("textcontent");
			   String dir=(String)session.getAttribute("Directory");
			   String information=(String)session.getAttribute("Infos");
				if(name==null)
				{ out.print("<p>There is no image!</p>");}
				
				String prefix = name.substring(name.lastIndexOf(".")+1);
				if(prefix.equals("tif"))
					out.print("<embed src=\"./temp/" + name + "\" type=\"image/tiff\" negative=yes>");
				else
					out.print("<img src=\"./temp/" + name + "\"/>");

				out.print("<p>" + information + "</p>");
				out.print("<p>" + dir + "</p>");
				
				try{
				File f = new File(dir, result);
				FileInputStream in = new FileInputStream(f);
				BufferedInputStream buffer=new BufferedInputStream(in);
				byte c[]=new byte[90];
				int n=0;
				while((n=buffer.read(c))!=-1)
				{	String temp=new String(c,0,n);
					out.print("<p>"+ temp + " </p>");
				}
				buffer.close();
				}
				catch(IOException e){
					out.print("<p> No result </p>");
				}
/*				
				try{
				File image = new File(dir, name);
				image.delete();
				out.print("<p> The image has been deleted. </p>");				
				}catch(Exception e){
					out.print("<p> Cannot delet the image! </p>");
				}
	*/			
			%>
			
			
			
			<%--try{ 	String tempFileName=(String)session.getId();
					File f1 = new File("./temp/",tempFileName);
					FileOutputStream o=new FileOutputStream(f1);
					InputStream in = request.getInputStream();
					byte b[]=new byte[10000];
					int n;
						while((n=in.read(b))!=-1)
						{ 	o.write(b, 0, n);	}
						o.close();
						in.close();
						RandomAccessFile random = new RandomAccessFile(f1,"r");
						int second = 1;
						String secondLine = null;
						while(second<=2)
						{	secondLine = random.readLine();
							second++;
						}
						int position = secondLine.lastIndexOf('\\');
						String fileName = secondLine.substring(position+1,secondLine.length()-1);
						random.seek(0);
						long forthEndPosition = 0;
						int forth=1;
						while((n=random.readByte())!=-1&&(forth<=4))
						{	if(n=='\n'){
								forthEndPosition=random.getFilePointer();
								forth++;
						}
						}
						random
			}
			--%>
			</div>
		</div>
</body>
</html>