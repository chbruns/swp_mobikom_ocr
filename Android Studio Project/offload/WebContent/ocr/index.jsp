<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/style.css" media="screen" rel="Stylesheet" type="text/css">
<title>Optical Character Recognition (OCR) Service</title>
		<script type="text/javascript">
			function fileOnChange() {
				if (document.getElementById("buttonChooseFile").value.length == 0) document.getElementById("buttonUpload").disabled = true; 
				else document.getElementById("buttonUpload").disabled = false;
			}
		</script>
</head>
<body>
		<div id="container">
			<div id="header">
				<h1>Android mobile devices computation offloading project</h1>
			</div>
			<div id="navigation">
				<ul>
					<li><a href="../index.jsp">Home</a></li>
					<li><a href="../management">Management</a></li>
					<li><a href="#">Ocr test</a></li>
					<li><a href="dictionary">Dictionary</a></li>
					<li><a href="../downloads">Downloads</a></li>
					<li><a href="../test" rel="nofollow">Test1</a></li>
					<li><a href="../run?algName=doSomeLoops&param1=20000000" rel="nofollow">Test2</a></li>
					<li><a href="../contact.jsp">Contact us</a></li>
					<li><a href="../logout">Logout</a></li>
				</ul>
			</div>
			<div id="content">
				<h2>Optical Character Recognition (OCR) Service</h2>
				<p>Upload Your Image for OCR service.</p>
				<p>Please select a image file from your file system containing the character you would like to be recognized.</p>
				<ul>
					<li>The "tif" format is prefered! Because the version of "tesseract" we use is 2.0. It can only recognize this format.</li>
					<li>If you upload other format images, we convert them firstly to the "tif" format and then recognize it. </li>
					<li>Unfortunately, the "tif" image cannot be shown on the result page, unless you install the add-on conponent to your browser. </li>
				</ul>
				<form action="uploadimage" enctype="multipart/form-data" method="post">
					<p>Device Type: <input type="text" name="devicekey"></p>
					<p>From:
						<select name="from">
						<option value="en">English</option>
						<option value="de">German</option>
						<option value="es">Spanish</option>
						<option value="fr">French</option>
						<option value="it">Italian</option>
						</select>
					</p>
					<p>To:
						<select name="to">
						<option value="none">No translation</option>
						<option value="en">English</option>
						<option value="de">German</option>
						<option value="es">Spanish</option>
						<option value="fr">French</option>
						<option value="it">Italian</option>
						</select>
					</p>
					<p><input id="buttonChooseFile" type="file" name="imagefile" size="40" onchange="fileOnChange()"></p>
					<p><input id="buttonUpload" type="submit" value="Upload" disabled></p>
				</form>
			</div>
			<div id="footer">Freie Universität Berlin, 2013</div>
		</div>
</body>
</html>