<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="UTF-8"%>

<%@ page import="java.sql.*" %>
<%@ page import="org.sqlite.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Insert title here</title>
    <head>
        <title>SQLite Demo</title>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <th>english</th>
                    <th>german</th>
                </tr>
            </thead>
            <tbody>
            
            
            <%
            
            
                Class.forName("org.sqlite.JDBC");
                Connection conn =
            
                       DriverManager.getConnection("jdbc:sqlite:/Users/florianmercks/Dropbox/FU-share/Vivi und Flow/SS 2014/Bachelorarbeit (Flow)/dictionary.sqlite");
                
                       if(conn != null) {
                    	   System.out.println("Connected to the database");
                    	   
                    	   Statement stat = conn.createStatement();
                    	   
                           ResultSet rs = stat.executeQuery("select * from dictionary;");
            
                           while (rs.next()) {
                               out.println("<tr>");
                               out.println("<td>" + rs.getString("english") + "</td>");
                               out.println("<td>" + rs.getString("german") + "</td>");
                               out.println("</tr>");
                           }
            
                           rs.close();
                           conn.close();
                       } // end if
                       
                
            %>
            </tbody>
        </table>
    </body>
</html>


