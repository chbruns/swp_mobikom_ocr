<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/style.css" media="screen" rel="Stylesheet" type="text/css">
<title>Dictionary to translate words between english and german</title>
		<script type="text/javascript">
			//function fileOnChange() {
			//	if (document.getElementById("buttonChooseFile").value.length == 0) document.getElementById("buttonUpload").disabled = true; 
			//	else document.getElementById("buttonUpload").disabled = false;
			//}
		</script>
		
		
		<script type="text/javascript">
			  function translateToEnglishMethod() {
				if (document.getElementById("buttonTranslateTo").value.length == 0) document.getElementById("buttonUpload").disabled = true; 
				else document.getElementById("buttonUpload").disabled = false;
			}
		</script>
		
		
		<script type="text/javascript">
			  function translateToGermanMethod() {
				if (document.getElementById("buttonChooseFile").value.length == 0) document.getElementById("buttonUpload").disabled = true; 
				else document.getElementById("buttonUpload").disabled = false;
			}
		</script>
		
		
		
</head>
<body>
		<div id="container">
			<div id="header">
				<h1>Android mobile devices computation offloading project</h1>
			</div>
			<div id="navigation">
				<ul>
					<li><a href="../index.jsp">Home</a></li>
					<li><a href="../management">Management</a></li>
					<li><a href="#">Ocr test</a></li>
					<li><a href="dictionary">Dictionary</a></li>
					<li><a href="../downloads">Downloads</a></li>
					<li><a href="../test" rel="nofollow">Test1</a></li>
					<li><a href="../run?algName=doSomeLoops&param1=20000000" rel="nofollow">Test2</a></li>
					<li><a href="../contact.jsp">Contact us</a></li>
					<li><a href="../logout">Logout</a></li>
				</ul>
			</div>
			<div id="content">
				<h2>Dictionary to translate words between English and German</h2>
				
				<p>In this dictionary you can either translate German words in English or the other way around.</p>
				<p> </p>
				<p> </p>
				<p>Choose the German word, which you would like to have translated in English</p>
				<form action="translateToEnglish" enctype="multipart/form-data" method="post">
					<p>german word: <input type="text" name="germanInputWord"></p>
					<p><input id="buttonTranslateToEnglish" type="button" value="translate" name="englishWord" size="40" onchange="translateToEnglishMethod()"></p>
					
				</form>
				
				<p>Choose the English word, which you would like to have translated in German</p>
				<form action="translateToGerman" enctype="multipart/form-data" method="post">
					<p>english word: <input type="text" name="englishInputWord"></p>
					<p><input id="buttonTranslateToGerman" type="button" value="translate" name="germanWord" size="40" onchange="translateToGermanMethod()"></p>
			
				</form>
				
				
			   <% 
			   String germanValue=(String)session.getAttribute("germanInputWord");
			   String englishValue=(String)session.getAttribute("englishInputWord");
			   
				if(germanValue==null)
				{ 
					//out.print("<p>Please type in a german word that you want to have translated!</p>")
				;}
				
				if(englishValue==null)
				{ 
					//out.print("<p>Please type in a english word that you want to have translated!</p>")
				;}
				
			   %>
				
				
			</div>
			<div id="footer">Freie Universität Berlin, 2013</div>
		</div>
</body>
</html>