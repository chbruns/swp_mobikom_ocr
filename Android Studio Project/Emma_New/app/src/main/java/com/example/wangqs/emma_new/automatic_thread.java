package com.example.wangqs.emma_new;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.util.List;

import static android.net.wifi.WifiManager.calculateSignalLevel;

/**
 * Created by mateusz 05.12.2014.
 */
public class automatic_thread implements Runnable {

    public static final String TAG = "automatic_thread";
    private boolean run;
    private File image;
    private String webURL;
    private Handler lHandler_t, mHandler_t, tHandler_t;

    private LocalRun local;
    private RemoteRun remote;
    private GetElement getelement;
    private String Recognized_Text;

    private boolean Activity_Connected;
    private int method;
    private int select;                                     // ADD 1 local 2 offload
    private String received;

    private Context current;

    public automatic_thread(File f, String url, Context context){
        image = f;
        webURL = url;

        current = context;
        run = false;

        Activity_Connected = false;
        method = 0;
        received = "";
        local = new LocalRun();
        remote = new RemoteRun();
        getelement = new GetElement();
    }

    public void end() {
        run = false;
    }

    public void Start_Connect(Handler l, Handler m, Handler t){
        lHandler_t = l;
        mHandler_t = m;
        tHandler_t = t;
        Activity_Connected = true;
    }

    public void Lost_Connect(){
        Activity_Connected = false;
    }


    @Override
    public void run() {

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE);
        select = selectConnection();

        switch(select) {
            case 1 :        // LOCAL
                try {
                    received = local.recognised(image);
                    Log.d(TAG,received);
                }catch(Exception e) {
                    Log.e(TAG,"Could not compute via local!");
                }
                break;

            case 2 :        // REMOTE
                try{
                    String received_t = remote.run(image,webURL,current,SystemClock.elapsedRealtime());
                    received = getelement.getElementValueFromXML(received_t, "Identifiedtext");
                    Log.d(TAG,received);
                }catch(Exception e){
                    Log.e(TAG,"Could not compute via offloading!");
                }
                break;
        }
        try{
            displayOutput(received);
        }catch(Exception e) {
            Log.e(TAG,"Error showing result on display!");
        }
    }

    public void displayOutput(String text) {
        Looper.prepare();
        Toast toast_o_1 = Toast.makeText(current, text, Toast.LENGTH_SHORT);
        toast_o_1.show();
        Looper.loop();
    }

    public int selectConnection() {
        ConnectivityManager connManager = (ConnectivityManager) current.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mMobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mWifi!=null && mWifi.isConnected()) {
            Log.i(TAG, "Wifi connection available");
            WifiManager wifiManager = (WifiManager) current.getSystemService(Context.WIFI_SERVICE);

            //wifiManager.startScan();
            List<ScanResult> scanResult = wifiManager.getScanResults();
            for (int i = 0; i < scanResult.size(); i++) {
                if(scanResult.get(i).BSSID.equals(wifiManager.getConnectionInfo().getBSSID())) {
                    int level = calculateSignalLevel(scanResult.get(i).level,100);
                    Log.d(TAG, "Speed of wifi"+level);
                    if( level <= 80) {                                      // IF LOW WIFI-CONNECTION, SWITCH TO MOBILE
                        if(mMobile!=null && mMobile.isAvailable()) {        // CHECK IF MOBILE AVAILABLE
                            wifiManager.disconnect();
                            Log.d(TAG, "Wifi disconnected. Mobile is also available!");
                        }
                    }
                }
            }
            Log.d(TAG, "Offload via wifi");
            return 2;

        } else if (mMobile!=null && mMobile.isConnected()) {
            Log.d(TAG,"Offload via mobile");
            return 2;

        }else {
            Log.d(TAG,"Local run");
            return 1;
        }
    }
}
