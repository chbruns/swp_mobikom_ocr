package com.example.wangqs.emma_new;

import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.memetix.mst.language.Language;

import java.io.IOException;


public class My_main extends ActionBarActivity {

//    my_service mService;
//    boolean mBound;

    private EditText Image_file;
    private EditText Number_Buckets;
    private EditText Update_Threshold;
    private EditText Remain_Percentage;
    public final static String EXTRA_MESSAGE = "Emma_main.Image_Name";
    private RadioGroup radioGroup;
    private RadioButton restart, offload;

    private Translator translator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_main);

        Image_file = (EditText) findViewById(R.id.Content);
        Number_Buckets = (EditText) findViewById(R.id.bucket_number);
        Update_Threshold = (EditText) findViewById(R.id.update_threshold);
        Remain_Percentage = (EditText) findViewById(R.id.remain_percentage);
        radioGroup = (RadioGroup) findViewById(R.id.radiogroup);
        restart = (RadioButton) findViewById(R.id.restart);
        offload = (RadioButton) findViewById(R.id.offloaded);
        radioGroup.setOnCheckedChangeListener(listen);


        // testing translation (use this wherever you want ;) )

        translator = new Translator(this);

        String testOfflineTranslation1 = translator.translate("Abarbeitung", Language.GERMAN, Language.ENGLISH);
        String testOfflineTranslation2 = translator.translate("Execution", Language.ENGLISH, Language.GERMAN);
        String testOnlineTranslation = translator.translate("Abarbeitung ist toll!", Language.GERMAN, Language.ENGLISH);
        String testOfflineTranslation3 = translator.translate("Schwuppdiewupp", Language.GERMAN, Language.ENGLISH);
    }


    @Override
    public void onResume() {
        super.onResume();





    }






    private RadioGroup.OnCheckedChangeListener listen = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId){
            switch(checkedId) {
                case R.id.restart:
                    Timestamp.type = 1;
                    break;
                case R.id.offloaded:
                    Timestamp.type = 2;
                    break;
            }
        }
    };

    public void StartIdentify(View view) {

        //test
 //       Intent intent_s = new Intent(this, my_service.class);

        //test end
        String message = Image_file.getText().toString();

        String bucket_number = Number_Buckets.getText().toString();
        int initial_number_of_buckets = Integer.parseInt(bucket_number);
        String update_threshold = Update_Threshold.getText().toString();
        int Account_of_update_threshold = Integer.parseInt(update_threshold);
        String remain_percentage = Remain_Percentage.getText().toString();
        int histogram_remain_percentage = Integer.parseInt(remain_percentage);
        Restart.initial_histogram(initial_number_of_buckets, Account_of_update_threshold, histogram_remain_percentage);

        Intent service = new Intent(this, my_service.class);
        service.putExtra(EXTRA_MESSAGE,message);
        startService(service);

        Intent intent = new Intent(this, my_display.class);
//        intent.putExtra(EXTRA_MESSAGE, message);
//        getApplicationContext().bindService(intent_s, mConnection, Context.BIND_AUTO_CREATE);
        startActivity(intent);
    }

    public void BindActivity(View view){
        Intent intent = new Intent(this, my_display.class);
//        intent.putExtra(EXTRA_MESSAGE, message);
//        getApplicationContext().bindService(intent_s, mConnection, Context.BIND_AUTO_CREATE);
        startActivity(intent);
    }

/*
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            my_service.LocalBinder binder = (my_service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;

        }
        @Override
        public void onServiceDisconnected(ComponentName name_end) {
            mBound = false;
        }
    };
*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
