package com.example.wangqs.emma_new;

/**
 * Created by christianbruns on 24.11.14.
 */


import android.content.Context;
import android.os.AsyncTask;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Translator {

    private static final String CLIENT_ID = "MSI-Translator";
    private static final String CLIENT_SECRET = "4gIvNg08mnbOuNofn+GxCQSWOyetmco26s6gnQu5owc=";

    private final Context context;
    DictionaryDbAdapter mDbHelper;

    public Translator(Context context){
        this.context = context;
        mDbHelper = new DictionaryDbAdapter(this.context);
        mDbHelper.createDatabase();
    }



    public String translateOnline(String str, Language from, Language to) {
        String translatedText = null;
        try {
            translatedText = new MsTranslatorAPI(){}.execute(str, from, to).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return translatedText;


    }

    public String translate(String toTranslate, Language from, Language to){
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(toTranslate);
        boolean found = matcher.find();

        // if toTranslate is one word only use offline dictionary else use MS-Translator-API
        if (!found) {
            if( (from.toString().equals("en") && to.toString().equals("de")) ||  (from.toString().equals("de") && to.toString().equals("en"))) {
                return translateOffline(toTranslate, from, to);
            }else {
                return translateOnline(toTranslate, from, to);
            }
        } else {
            return translateOnline(toTranslate, from, to);
        }
    }

    public String translateOffline(String str, Language from, Language to) {
        String translatedText;

        mDbHelper.open();

        translatedText = mDbHelper.dbQuery(str, from.name().toLowerCase(), to.name().toLowerCase());

        mDbHelper.close();

        if (translatedText == null) {
            translatedText = translateOnline(str, from, to);
        }

        return translatedText;
    }


    // AsyncTask for connecting msi-translator-api
    class MsTranslatorAPI extends AsyncTask<Object, Void, String>
    {
        @Override
        protected String doInBackground(Object... params) {
            Translate.setClientId(CLIENT_ID);
            Translate.setClientSecret(CLIENT_SECRET);

            String translatedText;
            try {
                translatedText = Translate.execute((String) params[0], (Language) params[1], (Language) params[2]);
            } catch(Exception e) {
                translatedText = e.toString();
            }
            return translatedText;
        }
    }


}
