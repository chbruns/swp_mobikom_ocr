package com.example.wangqs.emma_new;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import java.io.File;

/**
 * Created by wangqs on 7/9/14.
 */
public class main_thread implements Runnable {

    private boolean run;
    private File image;
    private String webURL;
    private saveData cun, cun_file;
    private Handler lHandler_t, mHandler_t, tHandler_t;
//    private Timestamp timestamp_t;
    private Restart restart_t;

    private offloading offloading;
    private local_restart local_restart;
    private LocalRun localrun;
//    private RemoteRun remoterun;
//    private boolean network_state;

    private Thread offloading_thread;
//    private ExecutorService cached_offloading_thread = Executors.newCachedThreadPool();
    private Thread local_restart_thread;


//    private String temp_t;
    private String Recognized_Text;
//    private Toast toast_t;

    private boolean Activity_Connected;
    private int method;

    private long startExperimentTime;
    private long ExperimentTime;
    private Context current;

    private static double originalBatteryCapacity;
    private BatteryUpdate battery;

    public main_thread(File f, String url, Context context){
        image = f;
        webURL = url;
        //      cun = new saveData();
        //      cun_file = new saveData();
        restart_t = new Restart();
        current = context;

//        timestamp_t = t;

        startExperimentTime = SystemClock.elapsedRealtime();
        String no_prefix = image.getName().substring(0, image.getName().lastIndexOf("."));
        String Data_file = no_prefix + "_" + "_" + String.valueOf(startExperimentTime);
        cun = new saveData();
        cun.writeDir(Data_file,image.getParent());
        String Content_file = "Recognised_of_" + "_" + no_prefix + "_" + String.valueOf(startExperimentTime);
        cun_file = new saveData();
        cun_file.writeDir(Content_file,image.getParent());

        battery = new BatteryUpdate(current, 1500);
        run = false;
//        network_state = false;

        localrun = new LocalRun();
//        remoterun = new RemoteRun();
//        temp_t = null;

        Activity_Connected = false;
        method = 0;

    }

    public void end() {
        run = false;
    }

    public void Start_Connect(Handler l, Handler m, Handler t){
        lHandler_t = l;
        mHandler_t = m;
        tHandler_t = t;
        Activity_Connected = true;
    }

    public void Lost_Connect(){
        Activity_Connected = false;
    }

    @Override
    public void run() {

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE);
        String notes;
        Initialization initialization = new Initialization(image, current);

        if (Activity_Connected) {
            method = 1;
            Bundle d = new Bundle();
            d.putInt("Method", method);
            Message signal_o_1 = lHandler_t.obtainMessage();
            signal_o_1.setData(d);
            signal_o_1.sendToTarget();
        }

        boolean Remote_Initialized  =   initialization.remote_Initialization(webURL);

        if(Activity_Connected){
            method = 0;
            Bundle dd = new Bundle();
            dd.putInt("Method", method);
            Message signal_o_2 = lHandler_t.obtainMessage();
            signal_o_2.setData(dd);
            signal_o_2.sendToTarget();

            notes = initialization.get_notes();
            Bundle t_2 = new Bundle();
            t_2.putString("Notification", notes);
            Message signal_t_2 = tHandler_t.obtainMessage();
            signal_t_2.setData(t_2);
            signal_t_2.sendToTarget();
        }

        if(Remote_Initialized){
            long temp = initialization.get_offloading_estimation();
            Timestamp.tau = 2*temp;
            Timestamp.Offloading_Expectation_Time = temp;
        }else{
            return;
        }

        if(Activity_Connected){
            method = 2;
            Bundle d = new Bundle();
            d.putInt("Method", method);
            Message signal_l_1 = lHandler_t.obtainMessage();
            signal_l_1.setData(d);
            signal_l_1.sendToTarget();
        }

        switch (Timestamp.type) {
            case 1:
                boolean Local_Initialized = initialization.local_Initialization();
                if (Local_Initialized) {
                    Timestamp.local_expectation = initialization.get_local_estimation();
                } else {
                    return;
                }
                break;

            case 2:
                Timestamp.local_expectation = (3*Timestamp.Offloading_Expectation_Time);
                break;
        }

        if (Activity_Connected) {
            method = 0;
            Bundle dd = new Bundle();
            dd.putInt("Method", method);
            Message signal_l_2 = lHandler_t.obtainMessage();
            signal_l_2.setData(dd);
            signal_l_2.sendToTarget();

            if(Timestamp.type==2){
                notes = "Offloading only: Local = 3 * Offloading";
            }else{
                notes = initialization.get_notes();
            }
            Bundle t_1 = new Bundle();
            t_1.putString("Notification", notes);
            Message signal_t_1 = tHandler_t.obtainMessage();
            signal_t_1.setData(t_1);
            signal_t_1.sendToTarget();
        }

        restart_t.initialLocalExecutionTime(Timestamp.local_expectation);

        restart_t.initialRemoteExecutionTime(Timestamp.Offloading_Expectation_Time);

        run = true;
        originalBatteryCapacity = battery.getCapacity();

        Timestamp.threshold = restart_t.initialization();

        do{
            Timestamp.Throughput++;
            long StartTime;
            long CompletedTime;
            long TransTime;
            long Waiting_Time = 0;
            long remote_period = 0;
            long Local_restart_start_time = 0;
            long restart_period_main_thread = 0;
            long restart_period_local_thread = 0;
            long t_tau = 0;
            boolean launch = false;
            long threshold = 0;
            Timestamp.completed_by = 0;
            Timestamp.state = "None";
            method = 0;
            int bar[] = new int[0];
//            synchronized (this){
//                temp_received = null;
//                temp_recognized = null;
                Timestamp.flag = false;
                Timestamp.succeed = false;
//                Timestamp.trigger = false;
                Timestamp.Network_State = 0;
//            }

//            network_state = remoterun.checkNetworkInfo();
//            network_state = RemoteRun.checkNetworkInfo();
            StartTime = SystemClock.elapsedRealtime();

//            if(network_state){

                if(Activity_Connected) {
                    method = 1;
                    Bundle d = new Bundle();
                    d.putInt("Method", method);
                    Message signal = lHandler_t.obtainMessage();
                    signal.setData(d);
                    signal.sendToTarget();
                }

                threshold = Timestamp.threshold;
                if((threshold > Timestamp.local_expectation)&&(Timestamp.Throughput>30)){
                    t_tau = Timestamp.tau;
                    Timestamp.trigger = true;
                }else{
                    t_tau = 30000;
                    Timestamp.trigger = false;
                }

                offloading = new offloading(image, webURL, current, StartTime);
                offloading_thread = new Thread(offloading);
                offloading_thread.start();

                while(!Timestamp.flag){

                    switch (Timestamp.type) {
                        case 1:
                            if (!launch) {
                                Waiting_Time = (SystemClock.elapsedRealtime() - StartTime);
                                if ((Waiting_Time > t_tau)||(Timestamp.Network_State==2)) {
                                    if (Activity_Connected) {
                                        method = 2;
                                        Bundle d_1 = new Bundle();
                                        d_1.putInt("Method", method);
                                        Message signal_1 = lHandler_t.obtainMessage();
                                        signal_1.setData(d_1);
                                        signal_1.sendToTarget();
                                    }
                                    local_restart = new local_restart(image);
                                    local_restart_thread = new Thread(local_restart);
                                    local_restart_thread.start();
                                    Local_restart_start_time = SystemClock.elapsedRealtime();
                                    launch = true;
                                }
                            } else {
                                //                          if(!(Local_run | Offload_run)){
                                restart_period_main_thread = SystemClock.elapsedRealtime() - Local_restart_start_time;
//                                restart_period_main_thread = (SystemClock.elapsedRealtime() - StartTime - Waiting_Time);
                                if(restart_period_main_thread < (2*Timestamp.local_expectation)){
                                    if ((!offloading_thread.isAlive()) && (!local_restart_thread.isAlive())) {
                                        if (!Timestamp.flag) {
                                            Timestamp.flag = true;
                                        }
                                    }
                                }else{
                                    if (!Timestamp.flag) {
                                        Timestamp.flag = true;
                                    }
                                }
                            }
                            break;
                        case 2:
                            Waiting_Time = (SystemClock.elapsedRealtime() - StartTime);
                            if((!offloading_thread.isAlive())||(Waiting_Time > (10*Timestamp.Offloading_Expectation_Time))){
                                if ((!Timestamp.flag)) {
                                    Timestamp.flag = true;
                                }
                            }
                            break;
                    }
                }

                if(Activity_Connected) {
                    method = 0;
                    Bundle d_3 = new Bundle();
                    d_3.putInt("Method", method);
                    Message signal_3 = lHandler_t.obtainMessage();
                    signal_3.setData(d_3);
                    signal_3.sendToTarget();
                }

                if(launch){
//                    restart_period_main_thread = SystemClock.elapsedRealtime() - Local_restart_start_time;
                    restart_period_local_thread = local_restart.Period_get();
                }
/*
            }else{
                try{
                    if(Activity_Connected) {
                        method = 2;
                        Bundle d_6 = new Bundle();
                        d_6.putInt("Method", method);
                        Message signal_6 = lHandler_t.obtainMessage();
                        signal_6.setData(d_6);
                        signal_6.sendToTarget();
                    }
                    temp_t = localrun.recognised(image);

                    Timestamp.state = "s_l_n_f";                              //local execution is successful when the network is failure
                    Timestamp.completed_by = 2;
                }catch (Exception e){
//                    completed_by = 1;
                    temp_t = "Local execution Failure!";
                    Timestamp.state = "f_l_n_f";                              //local execution is failure when the network is failure
                }
                if(Activity_Connected) {
                    method = 0;
                    Bundle d_7 = new Bundle();
                    d_7.putInt("Method", method);
                    Message signal_7 = lHandler_t.obtainMessage();
                    signal_7.setData(d_7);
                    signal_7.sendToTarget();
                }
            }
*/
            switch(Timestamp.completed_by){
                case 2:             //local execution
                    /*
                    if(!network_state)
                        Recognized_Text = temp_t;
                    else*/
                        Recognized_Text = local_restart.content_get();
                        remote_period = offloading.period_get();
                    Timestamp.succeed = true;
                    break;
                case 1:             //remote execution

                    Recognized_Text = offloading.content_get();
                    remote_period = offloading.period_get();
                    Timestamp.succeed = true;
                    break;
                case 0:
                    Recognized_Text = offloading.content_get();
                    break;
            }

            CompletedTime = SystemClock.elapsedRealtime();
            TransTime = CompletedTime - StartTime;

            //               OffloadingTime = Device_Get_Time - Device_Send_Time - RmExecutionTime;

            long histogram_update_start = SystemClock.elapsedRealtime();
            switch (Timestamp.completed_by){
                case 2://local
                    if(Timestamp.succeed){
                        long temp = TransTime - Waiting_Time;
                        if(temp > (2*Timestamp.local_expectation)){
                            if(restart_period_main_thread > restart_period_local_thread){
                                if(restart_period_local_thread < (2*Timestamp.local_expectation))
                                    Timestamp.local_expectation = restart_t.updateLocalExecutionTime(restart_period_local_thread);
                            }else{
                                if(restart_period_main_thread < (2*Timestamp.local_expectation))
                                    Timestamp.local_expectation = restart_t.updateLocalExecutionTime(restart_period_main_thread);
                            }
                        }else {
                            Timestamp.local_expectation = restart_t.updateLocalExecutionTime(temp);
                        }
                        TransTime = temp;
                    }
                    if((!Timestamp.trigger)&&(Timestamp.Network_State!=2)){
                        Timestamp.Offloading_Expectation_Time = restart_t.updateRemoteExecutionTime(Waiting_Time);
                        Timestamp.threshold = restart_t.Condition();
                        if(Timestamp.threshold > Timestamp.local_expectation)
                            Timestamp.tau = restart_t.updateRestartTime();
                    }
                    break;
                case 1://offload
                    if(Timestamp.succeed){
                        Timestamp.Offloading_Expectation_Time = restart_t.updateRemoteExecutionTime(TransTime);
                        Timestamp.threshold = restart_t.Condition();
                        if(Timestamp.threshold > Timestamp.local_expectation)
                            Timestamp.tau = restart_t.updateRestartTime();
                        bar = restart_t.update_chart();
                        Timestamp.timeoutConnection = (int)(2*Timestamp.Offloading_Expectation_Time);
                        Timestamp.timeoutSocket = (int)(3*Timestamp.Offloading_Expectation_Time);
                    }
                    break;
                case 0: //failure
                    break;
            }
            long histogram_update_finished = SystemClock.elapsedRealtime();
            long restart_cost = histogram_update_finished - histogram_update_start;

            Timestamp.currentBatteryVoltage = battery.getVoltage();
            Timestamp.currentBatteryCapacity = battery.getCapacity();
            Timestamp.presentBatteryCurrent = 0;
            Timestamp.Energy = Timestamp.currentBatteryCapacity - originalBatteryCapacity;

            String content;
            content = String.valueOf(Timestamp.Throughput) + "\t"
                    + String.valueOf(StartTime) + "\t"
                    + String.valueOf(threshold) + "\t"
                    + String.valueOf(t_tau) + "\t"
                    + String.valueOf(Timestamp.local_expectation) + "\t"
                    + String.valueOf(Waiting_Time) + "\t"
                    + String.valueOf(CompletedTime) + "\t"
                    + String.valueOf(TransTime) + "\t"
                    + String.valueOf(Timestamp.currentBatteryVoltage) + "\t"
                    + String.valueOf(Timestamp.currentBatteryCapacity) + "\t"
                    + String.valueOf(remote_period) + "\t"
                    + String.valueOf(restart_cost) + "\t"
                    + String.valueOf(restart_period_main_thread) + "\t"
                    + String.valueOf(restart_period_local_thread) + "\t"
                    + Timestamp.state + "\n";

            cun.Save(content);

            String rec_content;
            rec_content = String.valueOf(Timestamp.Throughput) + "\t"
                    + Recognized_Text + "\n";

            cun_file.Save(rec_content);

            if(Activity_Connected) {
                Message message = mHandler_t.obtainMessage();
                Bundle b = new Bundle();
                b.putString("Recognized_Word", Recognized_Text);
                b.putLong("Throughput_show", Timestamp.Throughput);
                b.putLong("Execution_Time", TransTime);
                b.putLong("Offloading_Time", Timestamp.threshold);
                b.putLong("Rm_Run_Time", Timestamp.tau);
                b.putLong("Current", Timestamp.presentBatteryCurrent);
                b.putDouble("Voltage", Timestamp.currentBatteryVoltage);
                b.putDouble("Energy_Consumption", Timestamp.Energy);
                b.putIntArray("Histogram", bar);
                b.putLong("Local_Expectation_Time", Timestamp.local_expectation);
                b.putLong("Offload_Expectation_Time", Timestamp.Offloading_Expectation_Time);
                message.setData(b);
                message.sendToTarget();
            }

//            if(network_state){
                if (launch) {
                    if (local_restart_thread.isAlive()) {
                        try {
                            local_restart_thread.join();
                        } catch (Exception e) {
                            if (Activity_Connected) {
                                notes = "Local Restart Thread Stop Exception";
                                Bundle t_3 = new Bundle();
                                t_3.putString("Notification", notes);
                                Message signal_t_3 = tHandler_t.obtainMessage();
                                signal_t_3.setData(t_3);
                                signal_t_3.sendToTarget();
                            }
                        }
                    }
                }

//            int Thread_Stop_Times = 0;

            while (offloading_thread.isAlive() && offloading_thread != null){
/*
                if(!offloading_thread.isInterrupted()){
                    offloading_thread.interrupt();
                }

                Thread_Stop_Times++;
                if(Thread_Stop_Times>3){
                    break;
                }
*/

                if (Activity_Connected) {
                    notes = "Offloading Thread is Stopping";
                    Bundle t_4 = new Bundle();
                    t_4.putString("Notification", notes);
                    Message signal_t_4 = tHandler_t.obtainMessage();
                    signal_t_4.setData(t_4);
                    signal_t_4.sendToTarget();
                }
//                    offloading_thread.interrupt();
//                    offloading.throw_exception();

//                    offloading_thread.interrupt();

                long ww = 1000;
                try{
                    offloading_thread.join(ww);
                }catch (InterruptedException e){
                    if (Activity_Connected) {
                        notes = "Offloading Thread throw Exception";
                        Bundle t_5 = new Bundle();
                        t_5.putString("Notification", notes);
                        Message signal_t_5 = tHandler_t.obtainMessage();
                        signal_t_5.setData(t_5);
                        signal_t_5.sendToTarget();
                    }
                }

                long tt = SystemClock.elapsedRealtime();
                long wt = 0;
                while (wt < 1000) {
                    wt = SystemClock.elapsedRealtime() - tt;
                }
            }
//           }


        }while (run);
        ExperimentTime = SystemClock.elapsedRealtime() - startExperimentTime;
        String Recognized_Text = "Experiment is Completed!";
        String content = "The experiment result is:" + "\r" + "Experiment time =" + String.valueOf(ExperimentTime)
                + "\t" + "Energy consumption = " + String.valueOf(Timestamp.Energy) + "\t" + "Throughput ="
                + String.valueOf(Timestamp.Throughput) + "\r";
        cun.Save(content);

        if(Activity_Connected) {
            Message message = mHandler_t.obtainMessage();
            Bundle c = new Bundle();
            c.putString("Recognized_Word", Recognized_Text);
            int bar[] = new int[20];
            c.putIntArray("Histogram", bar);
            message.setData(c);
            message.sendToTarget();
        }

    }
}
